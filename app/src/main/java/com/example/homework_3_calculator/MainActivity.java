package com.example.homework_3_calculator;

import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import java.text.DecimalFormat;
import com.example.homework_3_calculator.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {

    private ActivityMainBinding binding;
    private double valueOne = Double.NaN;
    private double valueTwo;

    private static final char ADDITION = '+';
    private static final char SUBTRACTION = '-';
    private static final char MULTIPLICATION = '*';
    private static final char DIVISION = '/';
    private static final char SQUAREROOT = '√';
    private static final char SQUARE = '^';
    private char CURRENT_ACTION;
    private DecimalFormat decimalFormat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        decimalFormat = new DecimalFormat("#.##########");
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        binding.buttonZero.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                binding.editText.setText(binding.editText.getText() + "0");
            }
        });

        binding.buttonOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                binding.editText.setText(binding.editText.getText() + "1");
            }
        });

        binding.buttonTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                binding.editText.setText(binding.editText.getText() + "2");
            }
        });

        binding.buttonThree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                binding.editText.setText(binding.editText.getText() + "3");
            }
        });

        binding.buttonFour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                binding.editText.setText(binding.editText.getText() + "4");
            }
        });

        binding.buttonFive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                binding.editText.setText(binding.editText.getText() + "5");
            }
        });

        binding.buttonSix.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                binding.editText.setText(binding.editText.getText() + "6");
            }
        });

        binding.buttonSeven.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                binding.editText.setText(binding.editText.getText() + "7");
            }
        });

        binding.buttonEight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                binding.editText.setText(binding.editText.getText() + "8");
            }
        });

        binding.buttonNine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                binding.editText.setText(binding.editText.getText() + "9");
            }
        });

        binding.buttonPeriod.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                binding.editText.setText(binding.editText.getText() + ".");
            }
        });

        binding.buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(CURRENT_ACTION == SQUAREROOT || CURRENT_ACTION == SQUARE){
                    CURRENT_ACTION = ADDITION;
                    computeNumber();
                    binding.textView.setText(decimalFormat.format(valueOne));
                    binding.editText.setText(null);
                    CURRENT_ACTION = 0;
                }
                else {
                    computeNumber();
                    CURRENT_ACTION = ADDITION;
                    binding.textView.setText(decimalFormat.format(valueOne) + "+");
                    binding.editText.setText(null);
                }
            }
        });

        binding.buttonMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(CURRENT_ACTION == SQUAREROOT || CURRENT_ACTION == SQUARE){
                    CURRENT_ACTION = SUBTRACTION;
                    computeNumber();
                    binding.textView.setText(decimalFormat.format(valueOne));
                    binding.editText.setText(null);
                    CURRENT_ACTION = 0;
                }
                else {
                    computeNumber();
                    CURRENT_ACTION = SUBTRACTION;
                    binding.textView.setText(decimalFormat.format(valueOne) + "-");
                    binding.editText.setText(null);
                }
            }
        });

        binding.buttonDivide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(CURRENT_ACTION == SQUAREROOT || CURRENT_ACTION == SQUARE){
                    CURRENT_ACTION = DIVISION;
                    computeNumber();
                    binding.textView.setText(decimalFormat.format(valueOne));
                    binding.editText.setText(null);
                    CURRENT_ACTION = 0;
                }
                else {
                    computeNumber();
                    CURRENT_ACTION = DIVISION;
                    binding.textView.setText(decimalFormat.format(valueOne) + "/");
                    binding.editText.setText(null);
                }
            }
        });

        binding.buttonMultiply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(CURRENT_ACTION == SQUAREROOT || CURRENT_ACTION == SQUARE){
                    CURRENT_ACTION = MULTIPLICATION;
                    computeNumber();
                    binding.textView.setText(decimalFormat.format(valueOne));
                    binding.editText.setText(null);
                    CURRENT_ACTION = 0;
                }
                else {
                    computeNumber();
                    CURRENT_ACTION = MULTIPLICATION;
                    binding.textView.setText(decimalFormat.format(valueOne) + "*");
                    binding.editText.setText(null);
                }
            }
        });

        binding.buttonSqrt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CURRENT_ACTION = SQUAREROOT;
                valueOne = Double.parseDouble(binding.editText.getText().toString());
                valueOne = Math.sqrt(valueOne);
                binding.textView.setText(decimalFormat.format(valueOne));
                binding.editText.setText(null);
            }
        });

        binding.buttonSquare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CURRENT_ACTION = SQUARE;
                valueOne = Double.parseDouble(binding.editText.getText().toString());
                valueOne = Math.pow(valueOne, 2);
                binding.textView.setText(decimalFormat.format(valueOne));
                binding.editText.setText(null);
            }
        });

        binding.buttonClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(binding.editText.getText().length() > 0){
                    CharSequence currentText = binding.editText.getText();
                    binding.editText.setText(currentText.subSequence(0, currentText.length()-1));
                }
                else{
                    valueOne = Double.NaN;
                    valueTwo = Double.NaN;
                    binding.editText.setText("");
                    binding.textView.setText("");
                }
            }
        });

        binding.buttonEqual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                computeNumber();
                binding.textView.setText(binding.textView.getText().toString() + decimalFormat.format(valueTwo) + " = " + decimalFormat.format(valueOne));
                valueOne = Double.NaN;
                CURRENT_ACTION = '0';
            }
        });
    }

    private void computeNumber(){
        if(!Double.isNaN(valueOne)){
            valueTwo = Double.parseDouble(binding.editText.getText().toString());
            binding.editText.setText(null);

            if(CURRENT_ACTION == ADDITION)
                valueOne = this.valueOne + valueTwo;
            else if(CURRENT_ACTION == SUBTRACTION)
                valueOne = this.valueOne - valueTwo;
            else if(CURRENT_ACTION == MULTIPLICATION)
                valueOne = this.valueOne * valueTwo;
            else if(CURRENT_ACTION == DIVISION)
                valueOne = this.valueOne / valueTwo;
        }
        else{
            try {
                valueOne = Double.parseDouble(binding.editText.getText().toString());
            } catch (Exception e) { }
        }
    }
}
